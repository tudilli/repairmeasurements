#!/bin/bash

MRRP=./MR-RePair/target/release/mrrp

TIME="/usr/bin/time -v"
TESTFOLDER=./compressed/$1MB

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB.mrrp) ;;
100) FILES=$(ls *.100MB.mrrp) ;;
200) FILES=$(ls *.200MB.mrrp) ;;
500) FILES=$(ls *.500MB.mrrp) ;;
1024) FILES=$(ls *.1024MB.mrrp) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ../..

for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $MRRP"
    $TIME -o ./logs/$1MB/MRRPDEC.$FILE.log $MRRP --input $TESTFOLDER/$FILE -d | tee ./outs/$1MB/MRRPDEC.$FILE.out
done
