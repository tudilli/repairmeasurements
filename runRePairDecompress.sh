#!/bin/bash

RP=./RePair/target/release/rp

TIME="/usr/bin/time -v"
TESTFOLDER=./compressed/$1MB

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB.rp) ;;
100) FILES=$(ls *.100MB.rp) ;;
200) FILES=$(ls *.200MB.rp) ;;
500) FILES=$(ls *.500MB.rp) ;;
1024) FILES=$(ls *.1024MB.rp) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ../..

for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $RP"
    $TIME -o ./logs/RPDEC.$FILE.log $RP --input $TESTFOLDER/$FILE -d | tee ./outs/RPDEC.$FILE.out
done
