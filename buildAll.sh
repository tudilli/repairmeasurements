#!/bin/bash
cd RePair
cargo update
cargo build --release
cd ..

cd MR-RePair
cargo update
cargo build --release
cd ..

cd Re-Pair-C
mkdir -p build && cd build
cmake ..
make
cd ../..

cd Re-Pair-Mem-Opt
mkdir -p build && cd build
cmake ..
make
cd ../..

cd IterativeRePair
mkdir -p build && cd build
cmake ../IterativeRePair/
make
cd ../..