#!/bin/bash

RP=./IterativeRePair/build/IterativeRePair/IterativeRePair

TIME="/usr/bin/time -v"
TESTFOLDER=./PizzaChili

mkdir -p logs/$1MB outs/$1MB $COMP/$1MB $DEC/$1MB

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB) ;;
100) FILES=$(ls *.100MB) ;;
200) FILES=$(ls *.200MB) ;;
500) FILES=$(ls *.500MB) ;;
1024) FILES=$(ls *.1024MB) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ..

for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $RP"
    $TIME -o ./logs/$1MB/IRPCOMP.$FILE.log $RP c9 $TESTFOLDER/$FILE $TESTFOLDER/$FILE.9.irp | tee ./outs/$1MB/IRPCOMP.$FILE.out
done
