#!/bin/bash

RP=./IterativeRePair/IterativeRePair/IterativeRePair

TIME="/usr/bin/time -v"
TESTFOLDER=./compressed/$1MB

mkdir -p logs/$1MB outs/$1MB $COMP/$1MB $DEC/$1MB

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB.irp) ;;
100) FILES=$(ls *.100MB.irp) ;;
200) FILES=$(ls *.200MB.irp) ;;
500) FILES=$(ls *.500MB.irp) ;;
1024) FILES=$(ls *.1024MB.irp) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ..

for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $RP"
    $TIME -o ./logs/$1MB/RPCOMP.$FILE.log $RP d2 ./compressed/$FILE $TESTFOLDER/$FILE.slp.dcp  | tee ./outs/$1MB/IRPDECSLP.$FILE.out
done
