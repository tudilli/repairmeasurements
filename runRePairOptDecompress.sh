#!/bin/bash

RPOPT=./Re-Pair-Mem-Opt/build/rp

TIME="/usr/bin/time -v"
TESTFOLDER=./compressed/$1MB

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB.rpo) ;;
100) FILES=$(ls *.100MB.rpo) ;;
200) FILES=$(ls *.200MB.rpo) ;;
500) FILES=$(ls *.500MB.rpo) ;;
1024) FILES=$(ls *.1024MB.rpo) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ../..

for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $RPOPT"
    $TIME -o ./logs/RPOPTDEC.$FILE.log $RPOPT d $TESTFOLDER/$FILE $TESTFOLDER/$FILE.opt | tee ./outs/RPOPTDEC.$FILE.out
done
