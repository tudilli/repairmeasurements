#!/bin/bash

TESTFOLDER=./PizzaChili
COMP=./compressed
DEC=./decompressed

mkdir -p logs outs

if [ "$2" = "all" ]
then
    ./runTests.sh 50
    ./runTests.sh 100
    ./runTests.sh 200
    ./runTests.sh 500
    ./runTests.sh 1024
elif [ "$2" = "50" ] || [ "$2" = "100" ] || [ "$2" = "200" ] || [ "$2" = "500" ] || [ "$2" = "1024" ]
then
    echo "Decompress testfiles..."
    gunzip -k $TESTFOLDER/*.$2MB.gz

    mkdir -p logs/$2MB outs/$2MB $COMP/$2MB $DEC/$2MB

    if [ "$1" = "c" ]
    then
        echo "Running Compression..."
        ./runRePair-C-Compress.sh $2
        ./runRePairCompress.sh $2
        ./runRePairOptCompress.sh $2 
        ./runMRRePairCompress.sh $2
        ./runIterativeRePairCompress3.sh $2
        ./runIterativeRePairCompress6.sh $2
        ./runIterativeRePairCompress.sh $2

        echo "Moving Results..."
        mv $TESTFOLDER/*$2MB.prel $COMP/$2MB/
        mv $TESTFOLDER/*$2MB.seq $COMP/$2MB/
        mv *$2MB.rpo $COMP/$2MB/
        mv $TESTFOLDER/*$2MB.rp $COMP/$2MB/
        mv $TESTFOLDER/*$2MB.mrrp $COMP/$2MB/
        mv $TESTFOLDER/*$2MB.irp $COMP/$2MB/

    elif [ "$1" = "d" ]
    then
        echo "Running Decompression..."
        ./runRePair-C-Decompress.sh $2
        ./runRePairDecompress.sh $2
        ./runRePairOptDecompress.sh $2 
        ./runMRRePairDecompress.sh $2
        ./runIterativerePairDecompressSLP.sh $2

        echo "Moving Results..."
        mv $COMP/$2MB/*.u $DEC/$2MB/
        mv $COMP/$2MB/*.opt $DEC/$2MB/
        mv $COMP/$2MB/*.dcp $DEC/$2MB/

        cd $TESTFOLDER
        case $2 in
            50) FILES=$(ls *.50MB) ;;
            100) FILES=$(ls *.100MB) ;;
            200) FILES=$(ls *.200MB) ;;
            500) FILES=$(ls *.500MB) ;;
            1024) FILES=$(ls *.1024MB) ;;
            *) echo "unknown parameter" && exit -1 ;;
        esac
        cd ..

        echo "" > $DEC/$2MB-Resulults.txt
        for FILE in ${FILES[@]}
        do
            diff -s $DEC/$2MB/$FILE.u PizzaChili/$FILE >> $DEC/$2MB-Resulults.txt
            diff -s $DEC/$2MB/$FILE.rpo.opt PizzaChili/$FILE >> $DEC/$2MB-Resulults.txt
            diff -s $DEC/$2MB/$FILE.rp.dcp PizzaChili/$FILE >> $DEC/$2MB-Resulults.txt
            diff -s $DEC/$2MB/$FILE.mrrp.dcp PizzaChili/$FILE >> $DEC/$2MB-Resulults.txt
            diff -s $DEC/$2MB/$FILE.irp.slp.dcp PizzaChili/$FILE >> $DEC/$2MB-Resulults.txt
        done
    fi

    rm $TESTFOLDER/*.$2MB

else
    echo "usage:"
    echo " runTests.sh <mode> <set>"
    echo " with <mode> is c for compress or d for decompress"
    echo " and <set> is all, 50, 100, 200 or 1024"
fi
