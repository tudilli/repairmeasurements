#!/bin/bash

RPCCOMP=./Re-Pair-C/build/src/repair
RPCDEC=./Re-Pair-C/build/src/despair

TIME="/usr/bin/time -v"
TESTFOLDER=./compressed/$1MB

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB) ;;
100) FILES=$(ls *.100MB) ;;
200) FILES=$(ls *.200MB) ;;
500) FILES=$(ls *.500MB) ;;
1024) FILES=$(ls *.1024MB) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ..



for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $RPCDEC"
    $TIME -o ./logs/$1MB/RPCDEC.$FILE.log $RPCDEC -i $TESTFOLDER/$FILE | tee ./outs/$1MB/RPCDEC.$FILE.out
done
