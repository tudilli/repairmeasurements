#!/bin/bash

RPOPT=./Re-Pair-Mem-Opt/build/rp

TIME="/usr/bin/time -v"
TESTFOLDER=./PizzaChili

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB) ;;
100) FILES=$(ls *.100MB) ;;
200) FILES=$(ls *.200MB) ;;
500) FILES=$(ls *.500MB) ;;
1024) FILES=$(ls *.1024MB) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ..

for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $RPOPT"
    $TIME -o ./logs/$1MB/RPOPTCOMP.$FILE.log $RPOPT c $TESTFOLDER/$FILE $FILE.rpo | tee ./outs/$1MB/RPOPTCOMP.$FILE.out
done
