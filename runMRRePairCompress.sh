#!/bin/bash

MRRP=./MR-RePair/target/release/mrrp

TIME="/usr/bin/time -v"
TESTFOLDER=./PizzaChili

cd $TESTFOLDER
case $1 in
50) FILES=$(ls *.50MB) ;;
100) FILES=$(ls *.100MB) ;;
200) FILES=$(ls *.200MB) ;;
500) FILES=$(ls *.500MB) ;;
1024) FILES=$(ls *.1024MB) ;;
*) echo "unknown parameter" && exit -1 ;;
esac
cd ..

for FILE in ${FILES[@]}
do
    echo "Running: $FILE with $MRRP"
    $TIME -o ./logs/$1MB/MRRPCOMP.$FILE.log $MRRP --input $TESTFOLDER/$FILE -c | tee ./outs/$1MB/MRRPCOMP.$FILE.out
done
